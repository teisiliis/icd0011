package config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

//Loads servlet and gives MvcConfig.class together with it.
public class Initializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[0];
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        //Searches for MvcConfig.class
        return new Class[] { Config.class, HsqlDataSource.class };
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] {"/api/*"};
    }
}
