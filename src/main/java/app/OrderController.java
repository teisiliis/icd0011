package app;

import model.Order;
import model.PaymentPart;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
public class OrderController {

    private CustomerOrderDao dao;

    public OrderController(CustomerOrderDao dao) {
        this.dao = dao;
    }

    //If get request, searches for this.
    @GetMapping("orders")
    public List<Order> getOrders() {
        return dao.getAllCustomerOrders();
    }

    //If get request, searches for this.
    @GetMapping("orders/{id}")
    public Order getOrderById(@PathVariable Long id) {
        return dao.getCustomerOrderById(id);
    }

    //If get request, searches for this.
    @GetMapping("orders/{id}/installments")
    public List<PaymentPart> getPaymentSchedule(
            @PathVariable Long id,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate start,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate end) {
        return dao.getOrderPaymentSchedule(id, start, end);
    }

    //If post request, searches for this.
    @PostMapping("orders")
    public Order createPost(@RequestBody @Valid Order order) {
        return dao.insertCustomerOrder(order);
    }

    //If delete request, searches for this.
    @DeleteMapping("orders/{id}")
    public void deleteOrder(@PathVariable Long id) {
        dao.deleteCustomerOrder(id);

    }
}
