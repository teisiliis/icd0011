package app;

import model.Order;
import model.OrderRow;
import model.PaymentPart;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerOrderDao {

    private JdbcTemplate template;

    public CustomerOrderDao(JdbcTemplate template) {
        this.template = template;
    }

    public List<Order> getAllCustomerOrders() {

        var mapper = new OrdersHandler();
        String sql = "SELECT order1.id, " +
                "order1.orderNumber, " +
                "orderRow.orderRowId, " +
                "orderRow.itemName, " +
                "orderRow.quantity, " +
                "orderRow.price, " +
                "orderRow.orderId " +
                "FROM order1 " +
                "LEFT JOIN orderRow ON order1.id = orderRow.orderId " +
                "ORDER BY order1.id ASC";

        template.query(sql, mapper);
        return mapper.getResult();
    }

    public Order getCustomerOrderById(Long id) {

        var mapper = new OrdersHandler();

        String sql = "SELECT order1.id, " +
                "order1.orderNumber, " +
                "orderRow.orderRowId, " +
                "orderRow.itemName, " +
                "orderRow.quantity, " +
                "orderRow.price, " +
                "orderRow.orderId " +
                "FROM order1 " +
                "LEFT JOIN orderRow ON order1.id = orderRow.orderId " +
                "WHERE order1.id = ?";

        template.query(sql, new Object[]{id}, mapper);

        List<Order> orders = mapper.getResult();
        if (orders.size() == 1) {
            return orders.get(0);
        } else {
            return null;
        }

    }

    public Order insertCustomerOrder(Order order) {
        var data = new BeanPropertySqlParameterSource(order);

        Number orderId = new SimpleJdbcInsert(template)
                .withTableName("order1")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(data);

        var orderRows = order.getOrderRows();

        for (OrderRow row: orderRows) {
            row.setOrderId(orderId.longValue());
            data = new BeanPropertySqlParameterSource(row);

            Number rowId = new SimpleJdbcInsert(template)
                    .withTableName("orderRow")
                    .usingGeneratedKeyColumns("orderRowId")
                    .executeAndReturnKey(data);
            row.setId(rowId.longValue());
        }

        order.setOrderRows(orderRows);
        order.setId(orderId.longValue());

        return order;
    }


    public void deleteCustomerOrder(Long id) {
        String sql = "DELETE FROM order1 WHERE id = ?";
        template.update(sql, id);
    }

    public List<PaymentPart> getOrderPaymentSchedule(Long id, LocalDate start, LocalDate end) {
        Order order = getCustomerOrderById(id);
        int totalSum = 0;
        //Get total sum of the order
        for (OrderRow row: order.getOrderRows()) {
            totalSum += row.getPrice() * row.getQuantity();
        }

        int monthsBetween = (int) ChronoUnit.MONTHS.between(
                YearMonth.from(start),
                YearMonth.from(end)
        );

        var paymentSize = totalSum / (monthsBetween + 1);
        if (paymentSize < 3) {
            monthsBetween = totalSum / 3 - 1;
            paymentSize = 3;
        }
        List<PaymentPart> parts = new ArrayList<>();
        parts.add(new PaymentPart(paymentSize, start));

        for (int i = 1; i < monthsBetween + 1; i++) {
            start = start.with(TemporalAdjusters.firstDayOfNextMonth());
            parts.add(new PaymentPart(paymentSize, start));
        }

        var modula = totalSum % (monthsBetween + 1);

        if (modula != 0) {
            for (int i = monthsBetween - modula + 1; i < monthsBetween + 1; i++) {
                var part = parts.get(i);
                var amount = part.getAmount() + 1;
                part.setAmount(amount);
                parts.set(i, part);
            }
        }

        return parts;
    }

    public Order addOrder(ResultSet rs) {
        try {
            Order order = new Order(rs.getLong("id"),
                    rs.getString("orderNumber"),
                    new ArrayList<>());
            List<OrderRow> orderRows = order.getOrderRows();
            orderRows.add(addOrderRow(rs));
            order.setOrderRows(orderRows);
            return order;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public OrderRow addOrderRow(ResultSet rs) {
        try {
            return new OrderRow(rs.getLong("orderRowId"),
                    rs.getString("itemName"),
                    rs.getInt("quantity"),
                    rs.getInt("price"),
                    rs.getLong("orderId"));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private class OrdersHandler implements RowCallbackHandler {
        private List<Order> orders = new ArrayList<>();

        public void processRow(ResultSet rs) throws SQLException {
            orders.add(addOrder(rs));
            while (rs.next()) {
                boolean exists = false;
                for (Order order : orders) {
                    if (order.getId() == rs.getLong("id")) {
                        exists = true;
                        var orderRows = order.getOrderRows();
                        orderRows.add(addOrderRow(rs));
                        order.setOrderRows(orderRows);
                    }
                }
                if (!exists) {
                    orders.add(addOrder(rs));
                }
            }
        }

        public List<Order> getResult() {
            return orders;
        }
    }
}
