package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderRow {
    private Long id;
    @NotNull
    @Size(min = 1)
    private String itemName;
    @NotNull
    @Min(1)
    private int quantity;
    @NotNull
    @Min(1)
    private int price;
    private Long orderId;
}
