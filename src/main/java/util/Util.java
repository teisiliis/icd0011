package util;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Util {

    public static String readStream(InputStream is) {
        try (Scanner scanner = new Scanner(is,
                StandardCharsets.UTF_8.name()).useDelimiter("\\A")) {

            return scanner.hasNext() ? scanner.next() : "";
        }
    }

    public static Map<String, String> convertJsonToMap (String json) {
        json = json.trim();
        String[] strs = json.substring(1, json.length() - 1).split("[,:]");
        Map<String, String> map = new LinkedHashMap<>();
        for (int i = 0; i < strs.length; i+=2) {
            String key = strs[i].trim();
            key = key.substring(1, key.length() - 1);
            String value = strs[i + 1].trim();
            if (key.length() <= 3 || key.equals("orderNumber")) {
                map.put(strs[i].trim(), strs[i + 1].trim());
            } else {
                try {
                    int number = Integer.parseInt(value) * 2;
                    map.put(strs[i].trim(), Integer.toString(number));
                } catch (Exception e) {
                    map.put(strs[i].trim(), strs[i].trim());
                }

            }
        }
        return map;
    }

    public static String mapToJson(Map<String, String> map) {
        StringBuilder json = new StringBuilder("{");
        for (Map.Entry<String, String> pair : map.entrySet()) {
            json.append(pair.getKey()).append(":").append(pair.getValue()).append(",");
        }
        json = new StringBuilder(json.substring(0, json.length() - 1));
        json.append("}");
        return json.toString();
    }

}
